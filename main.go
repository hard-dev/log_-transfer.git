package main

import (
	"fmt"
	"gitee.com/hard-dev/log_-transfer/conf"
	"gitee.com/hard-dev/log_-transfer/es"
	"gitee.com/hard-dev/log_-transfer/kafka"

	"gopkg.in/ini.v1"
)

//log transfer
//将日志数据从kafka取出来发往ES

func main() {
	//1.初始化 加载配置文件
	var cfg = new(conf.LogTransfer)
	err := ini.MapTo(cfg, "/conf/cfg.ini")
	if err != nil {
		fmt.Println("init config failed err:", err)
		return
	}
	//	kafka初始化 es初始化
	//2.发往es
	//初始化一个
	err = es.Init(cfg.ESCfg.Address)
	if err != nil {
		fmt.Println("init esconfig failed", err)
		return
	}
	fmt.Println("init es success")
	//连接kafka 创建分区的消费者
	//每个分区的消费者分别取出数据 通过SendToES()发往es
	err = kafka.Init([]string{cfg.KafkaCfg.Address}, cfg.Topic)
	if err != nil {
		fmt.Println("init kafka consumer failed", err)
		return
	}

	//从kafka中取日志数据
}
